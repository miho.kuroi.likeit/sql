﻿-- itemテーブル作成
CREATE TABLE item (
    item_id int PRIMARY KEY AUTO_INCREMENT, 
    item_name varchar(256), 
    item_price int, 
    category_id int
);
