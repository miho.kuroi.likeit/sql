﻿--itemテーブルとitem_categoryテーブルを結合する
--テーブルをcategory_id単位で集計
--category_name, item_price の合計(別名:total_price）を取得
--total_priceの値が大きな順にレコードを並び替え
SELECT category_name, SUM(item_price) AS total_price
FROM item_category
LEFT OUTER JOIN item
ON item_category.category_id = item.category_id
GROUP BY category_name
ORDER BY total_price DESC;

