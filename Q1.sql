﻿-- DB作成
CREATE DATABASE q1 DEFAULT CHARACTER SET utf8;

-- データベース使用状態
USE q1;

-- item_categoryテーブル作成
CREATE TABLE item_category (
    category_id int PRIMARY KEY AUTO_INCREMENT,
    category_name varchar(256)
);

